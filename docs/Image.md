# Image

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **str** | Image Description | [optional] 
**src** | **str** | https://myimagesource.com/myimagesource.jgp | [optional] 
**width** | **int** | Larger than 800 px | [optional] 
**height** | **int** | Larger than 800 px | [optional] 
**order** | **int** | 1 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

