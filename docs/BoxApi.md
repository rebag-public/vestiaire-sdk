# vestiaire_sdk.BoxApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_boxes**](BoxApi.md#get_boxes) | **GET** /api/box | Gets Boxes

# **get_boxes**
> get_boxes(id=id, name=name, category_id=category_id, limit=limit, order_by=order_by, offset=offset)

Gets Boxes

Gets the list of boxes

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.BoxApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific box to fetch (optional)
name = 'name_example' # str | The name of a specific box (like dustbag) (optional)
category_id = 'category_id_example' # str | The id of the category related to a list of boxes (example => category 33 has only Box 1 (dustbag) and Box 2 (certificate proof)) (optional)
limit = 'limit_example' # str | The limit of boxes to fetch (limit=10 to get only 10 first boxes) (optional)
order_by = 'order_by_example' # str | order by one specific field of the box (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets Boxes
    api_instance.get_boxes(id=id, name=name, category_id=category_id, limit=limit, order_by=order_by, offset=offset)
except ApiException as e:
    print("Exception when calling BoxApi->get_boxes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific box to fetch | [optional] 
 **name** | **str**| The name of a specific box (like dustbag) | [optional] 
 **category_id** | **str**| The id of the category related to a list of boxes (example &#x3D;&gt; category 33 has only Box 1 (dustbag) and Box 2 (certificate proof)) | [optional] 
 **limit** | **str**| The limit of boxes to fetch (limit&#x3D;10 to get only 10 first boxes) | [optional] 
 **order_by** | **str**| order by one specific field of the box (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

