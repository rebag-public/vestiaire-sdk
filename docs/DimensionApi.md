# vestiaire_sdk.DimensionApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_dimension_values**](DimensionApi.md#get_dimension_values) | **GET** /api/dimension | Gets dimension values

# **get_dimension_values**
> InlineResponse2003 get_dimension_values(id=id, name=name, category_id=category_id, limit=limit, order_by=order_by, offset=offset)

Gets dimension values

Gets dimension values

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.DimensionApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific dimension to fetch (optional)
name = 'name_example' # str | The name of a specific dimension (like width) (optional)
category_id = 'category_id_example' # str | The categoryId of a specific category to get the list of dimensions nedded for this category (like category 33 (bags) has only dimensions 1 (width), dimension 2 (height)) (optional)
limit = 'limit_example' # str | The limit of dimensions to fetch (limit=80 to get only 80 first dimensions) (optional)
order_by = 'order_by_example' # str | order by one specific field of the brand (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets dimension values
    api_response = api_instance.get_dimension_values(id=id, name=name, category_id=category_id, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DimensionApi->get_dimension_values: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific dimension to fetch | [optional] 
 **name** | **str**| The name of a specific dimension (like width) | [optional] 
 **category_id** | **str**| The categoryId of a specific category to get the list of dimensions nedded for this category (like category 33 (bags) has only dimensions 1 (width), dimension 2 (height)) | [optional] 
 **limit** | **str**| The limit of dimensions to fetch (limit&#x3D;80 to get only 80 first dimensions) | [optional] 
 **order_by** | **str**| order by one specific field of the brand (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

