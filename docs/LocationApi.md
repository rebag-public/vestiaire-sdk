# vestiaire_sdk.LocationApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_locations**](LocationApi.md#get_locations) | **GET** /api/location | Gets locations

# **get_locations**
> InlineResponse2005 get_locations(id=id, country=country, iso_code=iso_code, limit=limit, order_by=order_by, offset=offset)

Gets locations

Gets locations

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.LocationApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific location to fetch (optional)
country = 'country_example' # str | The country of a specific location (like France) (optional)
iso_code = 56 # int | The isoCode of a specific location (like FR) (optional)
limit = 'limit_example' # str | The limit of locations to fetch (limit=80 to get only 80 first locations) (optional)
order_by = 'order_by_example' # str | order by one specific field of the location (orderBy=country,desc, orderBy=isoCode,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets locations
    api_response = api_instance.get_locations(id=id, country=country, iso_code=iso_code, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling LocationApi->get_locations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific location to fetch | [optional] 
 **country** | **str**| The country of a specific location (like France) | [optional] 
 **iso_code** | **int**| The isoCode of a specific location (like FR) | [optional] 
 **limit** | **str**| The limit of locations to fetch (limit&#x3D;80 to get only 80 first locations) | [optional] 
 **order_by** | **str**| order by one specific field of the location (orderBy&#x3D;country,desc, orderBy&#x3D;isoCode,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

