# Meta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limit** | **int** | The limit that has been set in this request | [optional] 
**total** | **int** | The total number of items | [optional] 
**pages** | **int** | The number of pages for this request | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

