# vestiaire_sdk.SizeApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_sizes**](SizeApi.md#get_sizes) | **GET** /api/size | Gets sizes

# **get_sizes**
> InlineResponse2004 get_sizes(id=id, value=value, category_id=category_id, standard=standard, limit=limit, order_by=order_by, offset=offset)

Gets sizes

Gets sizes

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.SizeApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific size to fetch (optional)
value = 'value_example' # str | The value of a specific size (like XXL) (optional)
category_id = 56 # int | The categoryId of a specific category to get the list of sizes nedded for this category (like category 34 (clothes) has only sizes (FR, UK, International) (optional)
standard = 'standard_example' # str | The standard id of a specific size (example:to get the list of size for the standard FR and category 33 , categoryId=33&standard=56) (optional)
limit = 'limit_example' # str | The limit of sizes to fetch (limit=80 to get only 80 first sizes) (optional)
order_by = 'order_by_example' # str | order by one specific field of the size (orderBy=value,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets sizes
    api_response = api_instance.get_sizes(id=id, value=value, category_id=category_id, standard=standard, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SizeApi->get_sizes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific size to fetch | [optional] 
 **value** | **str**| The value of a specific size (like XXL) | [optional] 
 **category_id** | **int**| The categoryId of a specific category to get the list of sizes nedded for this category (like category 34 (clothes) has only sizes (FR, UK, International) | [optional] 
 **standard** | **str**| The standard id of a specific size (example:to get the list of size for the standard FR and category 33 , categoryId&#x3D;33&amp;standard&#x3D;56) | [optional] 
 **limit** | **str**| The limit of sizes to fetch (limit&#x3D;80 to get only 80 first sizes) | [optional] 
 **order_by** | **str**| order by one specific field of the size (orderBy&#x3D;value,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

