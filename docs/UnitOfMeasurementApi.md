# vestiaire_sdk.UnitOfMeasurementApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_units_of_measurement**](UnitOfMeasurementApi.md#get_units_of_measurement) | **GET** /api/unitsofmeasurement | Gets Unit of measurement

# **get_units_of_measurement**
> InlineResponse2008 get_units_of_measurement(id=id, name=name, limit=limit, order_by=order_by, offset=offset)

Gets Unit of measurement

Gets Unit of measurement

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.UnitOfMeasurementApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific Unit of measurement to fetch (optional)
name = 'name_example' # str | The name of a specific Unit of measurement (like cm) (optional)
limit = 'limit_example' # str | The limit of Unit of measurement to fetch (limit=2 to get only 2) (optional)
order_by = 'order_by_example' # str | order by one specific field of the Unit of measurement (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets Unit of measurement
    api_response = api_instance.get_units_of_measurement(id=id, name=name, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UnitOfMeasurementApi->get_units_of_measurement: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific Unit of measurement to fetch | [optional] 
 **name** | **str**| The name of a specific Unit of measurement (like cm) | [optional] 
 **limit** | **str**| The limit of Unit of measurement to fetch (limit&#x3D;2 to get only 2) | [optional] 
 **order_by** | **str**| order by one specific field of the Unit of measurement (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

