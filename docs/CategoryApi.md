# vestiaire_sdk.CategoryApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_categories**](CategoryApi.md#get_categories) | **GET** /api/category | Gets categories

# **get_categories**
> InlineResponse2002 get_categories(id=id, name=name, parent_id=parent_id, level=level, limit=limit, order_by=order_by, offset=offset)

Gets categories

Gets categories

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.CategoryApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific category to fetch (optional)
name = 'name_example' # str | The name of a specific category (like bags) (optional)
parent_id = 'parent_id_example' # str | The parent category id of a specific category (like 33 (bags) is the parentId of category 22 (handbags)) (optional)
level = 'level_example' # str | The level of the category to fetch (0 to get list of univers (Women, Men, Kids, Lifestyle ...), 1 to get the first level of categories (bags, clothes ...), 2 to get the list of last level of categories (handbags,Clutch bags, Backpacks ...), 3 to get the list of subcategories (Mini bags, Totes, Satchels ...)) (optional)
limit = 'limit_example' # str | The limit of categories to fetch (limit=80 to get only 80 first categories) (optional)
order_by = 'order_by_example' # str | order by one specific field of the category (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets categories
    api_response = api_instance.get_categories(id=id, name=name, parent_id=parent_id, level=level, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CategoryApi->get_categories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific category to fetch | [optional] 
 **name** | **str**| The name of a specific category (like bags) | [optional] 
 **parent_id** | **str**| The parent category id of a specific category (like 33 (bags) is the parentId of category 22 (handbags)) | [optional] 
 **level** | **str**| The level of the category to fetch (0 to get list of univers (Women, Men, Kids, Lifestyle ...), 1 to get the first level of categories (bags, clothes ...), 2 to get the list of last level of categories (handbags,Clutch bags, Backpacks ...), 3 to get the list of subcategories (Mini bags, Totes, Satchels ...)) | [optional] 
 **limit** | **str**| The limit of categories to fetch (limit&#x3D;80 to get only 80 first categories) | [optional] 
 **order_by** | **str**| order by one specific field of the category (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

