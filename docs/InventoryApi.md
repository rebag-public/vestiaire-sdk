# vestiaire_sdk.InventoryApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**update_inventory**](InventoryApi.md#update_inventory) | **PUT** /api/inventory | Updates inventory

# **update_inventory**
> update_inventory(body)

Updates inventory

Updates inventory. Put offline or online a product

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vestiaire_sdk.InventoryApi()
body = [vestiaire_sdk.Inventory()] # list[Inventory] | Updates inventory. Put offline or online a product

try:
    # Updates inventory
    api_instance.update_inventory(body)
except ApiException as e:
    print("Exception when calling InventoryApi->update_inventory: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[Inventory]**](Inventory.md)| Updates inventory. Put offline or online a product | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

