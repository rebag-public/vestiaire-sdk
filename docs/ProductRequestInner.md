# ProductRequestInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | [optional] 
**sku** | **str** |  | 
**size** | **object** |  | [optional] 
**attributes** | **list[object]** |  | 
**category** | **object** |  | 
**brand** | **object** |  | 
**images** | [**list[Image]**](Image.md) |  | 
**location** | **object** |  | 
**price** | **int** | 1000 | 
**currency** | **object** |  | [optional] 
**description** | **str** |  | 
**dimension_values** | **list[object]** |  | [optional] 
**tags** | [**list[Tag]**](Tag.md) |  | [optional] 
**serial_number** | **str** |  | [optional] 
**origin** | [**Origin**](Origin.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

