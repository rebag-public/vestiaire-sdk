# vestiaire_sdk.CommentApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_comment**](CommentApi.md#add_comment) | **POST** /api/comment | Add new comment
[**get_comment**](CommentApi.md#get_comment) | **GET** /api/comment | Gets list of comments

# **add_comment**
> add_comment(body)

Add new comment

Add new comment

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.CommentApi(vestiaire_sdk.ApiClient(configuration))
body = [vestiaire_sdk.Comment()] # list[Comment] | Comment data

try:
    # Add new comment
    api_instance.add_comment(body)
except ApiException as e:
    print("Exception when calling CommentApi->add_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[Comment]**](Comment.md)| Comment data | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_comment**
> InlineResponse202 get_comment(product_id=product_id)

Gets list of comments

Gets list of comments

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.CommentApi(vestiaire_sdk.ApiClient(configuration))
product_id = 56 # int | Product id (optional)

try:
    # Gets list of comments
    api_response = api_instance.get_comment(product_id=product_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CommentApi->get_comment: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **int**| Product id | [optional] 

### Return type

[**InlineResponse202**](InlineResponse202.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

