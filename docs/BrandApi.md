# vestiaire_sdk.BrandApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_brands**](BrandApi.md#get_brands) | **GET** /api/brand | Gets brands

# **get_brands**
> InlineResponse2001 get_brands(id=id, name=name, limit=limit, order_by=order_by, offset=offset)

Gets brands

Gets brands

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.BrandApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific brand to fetch (optional)
name = 'name_example' # str | The name of a specific brand (like Chanel) (optional)
limit = 'limit_example' # str | The limit of brands to fetch (limit=80 to get only 80 first brands) (optional)
order_by = 'order_by_example' # str | order by one specific field of the brand (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets brands
    api_response = api_instance.get_brands(id=id, name=name, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BrandApi->get_brands: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific brand to fetch | [optional] 
 **name** | **str**| The name of a specific brand (like Chanel) | [optional] 
 **limit** | **str**| The limit of brands to fetch (limit&#x3D;80 to get only 80 first brands) | [optional] 
 **order_by** | **str**| order by one specific field of the brand (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

