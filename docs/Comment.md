# Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**content** | **str** | The text body of the comment | [optional] 
**product** | **int** | The product id of the comment | [optional] 
**created_at** | **str** | The datetime of the comment, in Europe/Paris timezone | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

