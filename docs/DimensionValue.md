# DimensionValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dimension** | [**Dimension**](Dimension.md) |  | [optional] 
**value** | **int** | 1000 | [optional] 
**unit_of_measurement** | [**UnitOfMeasurement**](UnitOfMeasurement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

