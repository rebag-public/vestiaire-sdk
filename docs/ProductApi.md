# vestiaire_sdk.ProductApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_products**](ProductApi.md#add_products) | **POST** /api/product | Adds new Product
[**decrease_product_price**](ProductApi.md#decrease_product_price) | **PATCH** /api/product | Decrease product price
[**dislike_product**](ProductApi.md#dislike_product) | **PATCH** /api/product/{id}/dislike | Decrement product likes number
[**get_products**](ProductApi.md#get_products) | **GET** /api/product | Gets products
[**like_product**](ProductApi.md#like_product) | **PATCH** /api/product/{id}/like | Increment product likes number

# **add_products**
> add_products(body)

Adds new Product

Adds new product

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.ProductApi(vestiaire_sdk.ApiClient(configuration))
body = [vestiaire_sdk.ProductRequestInner()] # list[ProductRequestInner] | Product object

try:
    # Adds new Product
    api_instance.add_products(body)
except ApiException as e:
    print("Exception when calling ProductApi->add_products: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[ProductRequestInner]**](ProductRequestInner.md)| Product object | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **decrease_product_price**
> decrease_product_price(body)

Decrease product price

Decrease product price

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.ProductApi(vestiaire_sdk.ApiClient(configuration))
body = [vestiaire_sdk.DecreasePriceRequestInner()] # list[DecreasePriceRequestInner] | Product object

try:
    # Decrease product price
    api_instance.decrease_product_price(body)
except ApiException as e:
    print("Exception when calling ProductApi->decrease_product_price: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[DecreasePriceRequestInner]**](DecreasePriceRequestInner.md)| Product object | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: array
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dislike_product**
> Product dislike_product(id)

Decrement product likes number

Decrement the number of likes for this product

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.ProductApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | product id

try:
    # Decrement product likes number
    api_response = api_instance.dislike_product(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProductApi->dislike_product: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| product id | 

### Return type

[**Product**](Product.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_products**
> InlineResponse200 get_products(id=id, sku=sku, status=status, limit=limit, order_by=order_by, offset=offset)

Gets products

Gets products

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.ProductApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific product to fetch (optional)
sku = 'sku_example' # str | The sku of a specific product (like ext-123) (optional)
status = 'status_example' # str | The status of products (created, failed, pending) (optional)
limit = 'limit_example' # str | The limit of products to fetch (limit=80 to get only 80 first products) (optional)
order_by = 'order_by_example' # str | order by one specific field of the product (orderBy=createdAt,desc, orderBy=status,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets products
    api_response = api_instance.get_products(id=id, sku=sku, status=status, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProductApi->get_products: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific product to fetch | [optional] 
 **sku** | **str**| The sku of a specific product (like ext-123) | [optional] 
 **status** | **str**| The status of products (created, failed, pending) | [optional] 
 **limit** | **str**| The limit of products to fetch (limit&#x3D;80 to get only 80 first products) | [optional] 
 **order_by** | **str**| order by one specific field of the product (orderBy&#x3D;createdAt,desc, orderBy&#x3D;status,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **like_product**
> Product like_product(id)

Increment product likes number

Increment the number of likes for this product

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.ProductApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | product id

try:
    # Increment product likes number
    api_response = api_instance.like_product(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProductApi->like_product: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| product id | 

### Return type

[**Product**](Product.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

