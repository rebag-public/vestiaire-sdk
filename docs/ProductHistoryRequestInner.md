# ProductHistoryRequestInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | **int** | The id of VC product. | 
**action** | **int** | The id of the VC product action. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

