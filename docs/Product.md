# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**vc_id** | **int** |  | [optional] 
**title** | **str** |  | [optional] 
**sku** | **str** |  | [optional] 
**size** | [**Size**](Size.md) |  | [optional] 
**attributes** | [**list[Attribute]**](Attribute.md) |  | [optional] 
**category** | [**Category**](Category.md) |  | [optional] 
**brand** | [**Brand**](Brand.md) |  | [optional] 
**images** | [**list[Image]**](Image.md) |  | [optional] 
**location** | [**Location**](Location.md) |  | [optional] 
**status** | **str** | CREATED|PENDING|REJECTED|ACCEPTED|ONLINE|SOLD|RESERVED|OFFLINE | [optional] 
**price** | **int** | 1000 | [optional] 
**currency** | [**Currency**](Currency.md) |  | [optional] 
**description** | **str** |  | [optional] 
**created_at** | **str** | DD/MM/YYYY hh:mm:ss | [optional] 
**dimension_values** | [**list[DimensionValue]**](DimensionValue.md) |  | [optional] 
**tags** | [**list[Tag]**](Tag.md) |  | [optional] 
**serial_number** | **str** |  | [optional] 
**origin** | [**Origin**](Origin.md) |  | [optional] 
**likes** | **int** | 12 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

