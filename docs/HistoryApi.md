# vestiaire_sdk.HistoryApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_product_history**](HistoryApi.md#add_product_history) | **POST** /api/product/history | Adds history to Product
[**get_histories**](HistoryApi.md#get_histories) | **GET** /api/product/history | Get status histories

# **add_product_history**
> add_product_history(body)

Adds history to Product

Adds new product

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.HistoryApi(vestiaire_sdk.ApiClient(configuration))
body = [vestiaire_sdk.ProductHistoryRequestInner()] # list[ProductHistoryRequestInner] | History action

try:
    # Adds history to Product
    api_instance.add_product_history(body)
except ApiException as e:
    print("Exception when calling HistoryApi->add_product_history: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**list[ProductHistoryRequestInner]**](ProductHistoryRequestInner.md)| History action | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_histories**
> InlineResponse2009 get_histories(product_id, limit=limit, order_by=order_by, offset=offset)

Get status histories

Gets the list of statuses for a specific product.

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.HistoryApi(vestiaire_sdk.ApiClient(configuration))
product_id = 56 # int | id of a specific product to fetch
limit = 'limit_example' # str | The limit of categories to fetch (limit=80 to get only 80 first categories) (optional)
order_by = 'order_by_example' # str | order by one specific field of the category (orderBy=label,desc, orderBy=id,asc). Default is \"createdAt,desc\" (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Get status histories
    api_response = api_instance.get_histories(product_id, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling HistoryApi->get_histories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **product_id** | **int**| id of a specific product to fetch | 
 **limit** | **str**| The limit of categories to fetch (limit&#x3D;80 to get only 80 first categories) | [optional] 
 **order_by** | **str**| order by one specific field of the category (orderBy&#x3D;label,desc, orderBy&#x3D;id,asc). Default is \&quot;createdAt,desc\&quot; | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

