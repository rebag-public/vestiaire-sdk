# vestiaire_sdk.AttributeApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_attributes**](AttributeApi.md#get_attributes) | **GET** /api/attribute | Gets attributes

# **get_attributes**
> InlineResponse2007 get_attributes(id=id, name=name, value=value, category_id=category_id, brand_id=brand_id, category_vc_id=category_vc_id, brand_vc_id=brand_vc_id, limit=limit, order_by=order_by, offset=offset)

Gets attributes

Gets attributes

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.AttributeApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific attribute to fetch (optional)
name = 'name_example' # str | The name of a specific attribute (options => color, model, material, condition, pattern, material_watch_strap, watch_mechanism) (optional)
value = 'value_example' # str | The value of a specific attribute (like red (to get color red)) (optional)
category_id = 56 # int | The categoryId of a specific category to get the list of attributes nedded for this category (like category 41 (Watches) has only attributes (model, condition, material,material_watch_strap, watch_mechanism, color) (optional)
brand_id = 56 # int | The brandId of a specific Brand to get the attributes corresponding the duo (brand, category) (optional)
category_vc_id = 56 # int | The categoryVcId of a specific category to get the attributes corresponding the duo (VcBrand, VcCategory), only for super_admin (optional)
brand_vc_id = 56 # int | The brandVcId of a specific Brand to get the attributes corresponding the duo (Vcbrand, Vccategory), only for super_admin (optional)
limit = 'limit_example' # str | The limit of attributes to fetch (limit=80 to get only 80 first attributes) (optional)
order_by = 'order_by_example' # str | order by one specific field of the attribute (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets attributes
    api_response = api_instance.get_attributes(id=id, name=name, value=value, category_id=category_id, brand_id=brand_id, category_vc_id=category_vc_id, brand_vc_id=brand_vc_id, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeApi->get_attributes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific attribute to fetch | [optional] 
 **name** | **str**| The name of a specific attribute (options &#x3D;&gt; color, model, material, condition, pattern, material_watch_strap, watch_mechanism) | [optional] 
 **value** | **str**| The value of a specific attribute (like red (to get color red)) | [optional] 
 **category_id** | **int**| The categoryId of a specific category to get the list of attributes nedded for this category (like category 41 (Watches) has only attributes (model, condition, material,material_watch_strap, watch_mechanism, color) | [optional] 
 **brand_id** | **int**| The brandId of a specific Brand to get the attributes corresponding the duo (brand, category) | [optional] 
 **category_vc_id** | **int**| The categoryVcId of a specific category to get the attributes corresponding the duo (VcBrand, VcCategory), only for super_admin | [optional] 
 **brand_vc_id** | **int**| The brandVcId of a specific Brand to get the attributes corresponding the duo (Vcbrand, Vccategory), only for super_admin | [optional] 
 **limit** | **str**| The limit of attributes to fetch (limit&#x3D;80 to get only 80 first attributes) | [optional] 
 **order_by** | **str**| order by one specific field of the attribute (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

