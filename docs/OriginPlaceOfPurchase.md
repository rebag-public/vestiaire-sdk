# OriginPlaceOfPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 1 &#x3D; shop, 2 &#x3D; Private solds, 3 &#x3D; Vestiaire Collective, 4 &#x3D; other | [optional] 
**description** | **str** | amazon | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

