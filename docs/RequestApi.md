# vestiaire_sdk.RequestApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_request**](RequestApi.md#get_request) | **GET** /api/request | Gets the request details

# **get_request**
> get_request(id=id)

Gets the request details

Gets request details, progress, status, errors

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.RequestApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific request to fetch (optional)

try:
    # Gets the request details
    api_instance.get_request(id=id)
except ApiException as e:
    print("Exception when calling RequestApi->get_request: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific request to fetch | [optional] 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

