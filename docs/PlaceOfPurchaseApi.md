# vestiaire_sdk.PlaceOfPurchaseApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_place_of_purchase**](PlaceOfPurchaseApi.md#get_place_of_purchase) | **GET** /api/placeofpurchase | Gets places of purchases

# **get_place_of_purchase**
> get_place_of_purchase(id=id, description=description, limit=limit, order_by=order_by, offset=offset)

Gets places of purchases

Gets the list of places of purchase

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.PlaceOfPurchaseApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific place of purchase to fetch (optional)
description = 'description_example' # str | The description of a specific place of purchase (like 'store') (optional)
limit = 'limit_example' # str | The limit of place of purchase to fetch (limit=10 to get only 10) (optional)
order_by = 'order_by_example' # str | order by one specific field of the place of purchase (orderBy=description,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets places of purchases
    api_instance.get_place_of_purchase(id=id, description=description, limit=limit, order_by=order_by, offset=offset)
except ApiException as e:
    print("Exception when calling PlaceOfPurchaseApi->get_place_of_purchase: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific place of purchase to fetch | [optional] 
 **description** | **str**| The description of a specific place of purchase (like &#x27;store&#x27;) | [optional] 
 **limit** | **str**| The limit of place of purchase to fetch (limit&#x3D;10 to get only 10) | [optional] 
 **order_by** | **str**| order by one specific field of the place of purchase (orderBy&#x3D;description,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

