# vestiaire_sdk.TagApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_tags**](TagApi.md#get_tags) | **GET** /api/tag | Gets tags

# **get_tags**
> InlineResponse2006 get_tags(id=id, name=name, limit=limit, order_by=order_by, offset=offset)

Gets tags

Gets the list of tags like vintage

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint


# create an instance of the API class
api_instance = vestiaire_sdk.TagApi(vestiaire_sdk.ApiClient(configuration))
id = 56 # int | id of a specific tag to fetch (optional)
name = 'name_example' # str | The name of a specific tag (like vintage) (optional)
limit = 'limit_example' # str | The limit of tags to fetch (limit=10 to get only 10 first tags) (optional)
order_by = 'order_by_example' # str | order by one specific field of the tag (orderBy=name,desc, orderBy=id,asc) (optional)
offset = 'offset_example' # str | the position of the first result to retrieve (offset=10) (optional)

try:
    # Gets tags
    api_response = api_instance.get_tags(id=id, name=name, limit=limit, order_by=order_by, offset=offset)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TagApi->get_tags: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id of a specific tag to fetch | [optional] 
 **name** | **str**| The name of a specific tag (like vintage) | [optional] 
 **limit** | **str**| The limit of tags to fetch (limit&#x3D;10 to get only 10 first tags) | [optional] 
 **order_by** | **str**| order by one specific field of the tag (orderBy&#x3D;name,desc, orderBy&#x3D;id,asc) | [optional] 
 **offset** | **str**| the position of the first result to retrieve (offset&#x3D;10) | [optional] 

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

