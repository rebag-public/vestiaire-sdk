# vestiaire_sdk.AuthenticationApi

All URIs are relative to *https://{host}*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_token**](AuthenticationApi.md#get_token) | **POST** /login_check | get a new authentication token
[**register**](AuthenticationApi.md#register) | **POST** /api/register | create seller account

# **get_token**
> LoginCheckResponse get_token(body)

get a new authentication token

get a new authentication token

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vestiaire_sdk.AuthenticationApi()
body = vestiaire_sdk.LoginCheck() # LoginCheck | seller credentials

try:
    # get a new authentication token
    api_response = api_instance.get_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthenticationApi->get_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**LoginCheck**](LoginCheck.md)| seller credentials | 

### Return type

[**LoginCheckResponse**](LoginCheckResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register**
> register(body)

create seller account

create seller account

### Example
```python
from __future__ import print_function
import time
import vestiaire_sdk
from vestiaire_sdk.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = vestiaire_sdk.AuthenticationApi()
body = vestiaire_sdk.Register() # Register | seller credentials

try:
    # create seller account
    api_instance.register(body)
except ApiException as e:
    print("Exception when calling AuthenticationApi->register: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Register**](Register.md)| seller credentials | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: object
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

