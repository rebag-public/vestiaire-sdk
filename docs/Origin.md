# Origin

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invoice** | **str** | Invoice file url | [optional] 
**price** | **int** | 1000 | [optional] 
**currency** | [**OriginCurrency**](OriginCurrency.md) |  | [optional] 
**_date** | **str** | DD/MM/YYYY hh:mm:ss | [optional] 
**packaging** | [**OriginPackaging**](OriginPackaging.md) |  | [optional] 
**place_of_purchase** | [**OriginPlaceOfPurchase**](OriginPlaceOfPurchase.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

