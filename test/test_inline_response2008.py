# coding: utf-8

"""
    B2C API

    B2C API  # noqa: E501

    OpenAPI spec version: 0.0.36
    Contact: tech@vestiairecollective.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import vestiaire_sdk
from models.inline_response2008 import InlineResponse2008  # noqa: E501
from vestiaire_sdk.rest import ApiException


class TestInlineResponse2008(unittest.TestCase):
    """InlineResponse2008 unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testInlineResponse2008(self):
        """Test InlineResponse2008"""
        # FIXME: construct object with mandatory attributes with example values
        # model = vestiaire_sdk.models.inline_response2008.InlineResponse2008()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
