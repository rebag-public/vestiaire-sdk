# coding: utf-8

"""
    B2C API

    B2C API  # noqa: E501

    OpenAPI spec version: 0.0.36
    Contact: tech@vestiairecollective.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class Origin(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'invoice': 'str',
        'price': 'int',
        'currency': 'OriginCurrency',
        '_date': 'str',
        'packaging': 'OriginPackaging',
        'place_of_purchase': 'OriginPlaceOfPurchase'
    }

    attribute_map = {
        'invoice': 'invoice',
        'price': 'price',
        'currency': 'currency',
        '_date': 'date',
        'packaging': 'packaging',
        'place_of_purchase': 'placeOfPurchase'
    }

    def __init__(self, invoice=None, price=None, currency=None, _date=None, packaging=None, place_of_purchase=None):  # noqa: E501
        """Origin - a model defined in Swagger"""  # noqa: E501
        self._invoice = None
        self._price = None
        self._currency = None
        self.__date = None
        self._packaging = None
        self._place_of_purchase = None
        self.discriminator = None
        if invoice is not None:
            self.invoice = invoice
        if price is not None:
            self.price = price
        if currency is not None:
            self.currency = currency
        if _date is not None:
            self._date = _date
        if packaging is not None:
            self.packaging = packaging
        if place_of_purchase is not None:
            self.place_of_purchase = place_of_purchase

    @property
    def invoice(self):
        """Gets the invoice of this Origin.  # noqa: E501

        Invoice file url  # noqa: E501

        :return: The invoice of this Origin.  # noqa: E501
        :rtype: str
        """
        return self._invoice

    @invoice.setter
    def invoice(self, invoice):
        """Sets the invoice of this Origin.

        Invoice file url  # noqa: E501

        :param invoice: The invoice of this Origin.  # noqa: E501
        :type: str
        """

        self._invoice = invoice

    @property
    def price(self):
        """Gets the price of this Origin.  # noqa: E501

        1000  # noqa: E501

        :return: The price of this Origin.  # noqa: E501
        :rtype: int
        """
        return self._price

    @price.setter
    def price(self, price):
        """Sets the price of this Origin.

        1000  # noqa: E501

        :param price: The price of this Origin.  # noqa: E501
        :type: int
        """

        self._price = price

    @property
    def currency(self):
        """Gets the currency of this Origin.  # noqa: E501


        :return: The currency of this Origin.  # noqa: E501
        :rtype: OriginCurrency
        """
        return self._currency

    @currency.setter
    def currency(self, currency):
        """Sets the currency of this Origin.


        :param currency: The currency of this Origin.  # noqa: E501
        :type: OriginCurrency
        """

        self._currency = currency

    @property
    def _date(self):
        """Gets the _date of this Origin.  # noqa: E501

        DD/MM/YYYY hh:mm:ss  # noqa: E501

        :return: The _date of this Origin.  # noqa: E501
        :rtype: str
        """
        return self.__date

    @_date.setter
    def _date(self, _date):
        """Sets the _date of this Origin.

        DD/MM/YYYY hh:mm:ss  # noqa: E501

        :param _date: The _date of this Origin.  # noqa: E501
        :type: str
        """

        self.__date = _date

    @property
    def packaging(self):
        """Gets the packaging of this Origin.  # noqa: E501


        :return: The packaging of this Origin.  # noqa: E501
        :rtype: OriginPackaging
        """
        return self._packaging

    @packaging.setter
    def packaging(self, packaging):
        """Sets the packaging of this Origin.


        :param packaging: The packaging of this Origin.  # noqa: E501
        :type: OriginPackaging
        """

        self._packaging = packaging

    @property
    def place_of_purchase(self):
        """Gets the place_of_purchase of this Origin.  # noqa: E501


        :return: The place_of_purchase of this Origin.  # noqa: E501
        :rtype: OriginPlaceOfPurchase
        """
        return self._place_of_purchase

    @place_of_purchase.setter
    def place_of_purchase(self, place_of_purchase):
        """Sets the place_of_purchase of this Origin.


        :param place_of_purchase: The place_of_purchase of this Origin.  # noqa: E501
        :type: OriginPlaceOfPurchase
        """

        self._place_of_purchase = place_of_purchase

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Origin, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Origin):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
